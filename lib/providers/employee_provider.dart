import 'package:flutter/foundation.dart';
import 'package:mvvm/models/dto/pagination.dart';
import 'package:mvvm/models/employee.dart';
import 'package:mvvm/services/employee_service.dart';

enum EmployeeState { initial, loading, completed, error }

class EmployeeProvider extends ChangeNotifier {
  EmployeeProvider()
      : _state = EmployeeState.initial,
        _list = [],
        _pagination = Pagination(offset: 0, limit: 10),
        _isLastPage = false;

  EmployeeState _state;

  final Pagination _pagination;
  final List<Employee> _list;
  bool _isLastPage;

  EmployeeState get state => _state;

  Pagination get pagination => _pagination;

  List<String> get keys => _list.map((e) => e.id).toList();

  List<Employee> get employees => _list;

  Future<void> fetchEmployees(Pagination pagination) async {
    try {
      if (pagination.offset == 0) {
        _state = EmployeeState.loading;
        _isLastPage = false;
        notifyListeners();
      }

      if (_isLastPage) {
        return;
      }

      _pagination.apply(offset: pagination.offset, limit: pagination.limit);

      final list = await EmployeeService.fetchEmployees(_pagination);
      if (list.isEmpty) {
        _isLastPage = true;
      } else if (_pagination.offset != 0) {
        list.retainWhere((e) => !keys.contains(e.id));
        _list.addAll(list);
      } else {
        _list..clear()..addAll(list);
      }

      _state = EmployeeState.completed;
    } catch(e) {
      if (kDebugMode) {
        print(e);
      }

      _state = EmployeeState.error;
    }

    notifyListeners();
  }

  @override
  dispose() {
    _list.clear();

    super.dispose();
  }
}