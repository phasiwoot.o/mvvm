// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'employee.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Employee _$EmployeeFromJson(Map<String, dynamic> json) => Employee(
      id: Employee._idFromJson(json['id'] as Map<String, dynamic>),
      name: Employee._nameFromJson(json['name'] as Map<String, dynamic>),
      gender: json['gender'] as String,
      email: json['email'] as String,
      dob: json['dob'] as Map<String, dynamic>?,
      phone: json['phone'] as String?,
      cell: json['cell'] as String?,
      picture: json['picture'] as Map<String, dynamic>?,
    );

Map<String, dynamic> _$EmployeeToJson(Employee instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'gender': instance.gender,
      'email': instance.email,
      'dob': instance.dob,
      'phone': instance.phone,
      'cell': instance.cell,
      'picture': instance.picture,
    };
