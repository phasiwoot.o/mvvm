import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'employee.g.dart';

@JsonSerializable()
class Employee extends Equatable {
  const Employee({
    required this.id,
    required this.name,
    required this.gender,
    required this.email,
    this.dob,
    this.phone,
    this.cell,
    this.picture,
  });

  @JsonKey(fromJson: _idFromJson)
  final String id;
  @JsonKey(fromJson: _nameFromJson)
  final String name;
  final String gender;
  final String email;
  final Map<String,dynamic>? dob;
  final String? phone;
  final String? cell;
  final Map<String,dynamic>? picture;

  factory Employee.fromJson(Map<String, dynamic> json) => _$EmployeeFromJson(json);

  Map<String, dynamic> toJson() => _$EmployeeToJson(this);

  static String _idFromJson(Map<String, dynamic> value) {
    return '${value['name'] ?? ''}-${value['value'] ?? ''}';
  }

  static String _nameFromJson(Map<String, dynamic> value) {
    return '${value['title'] ?? ''} ${value['first'] ?? ''}  ${value['last'] ?? ''}';
  }

  Uri? get imageUrl => Uri.tryParse(picture?['medium'] ?? '');

  Uri? get thumbnailUrl => Uri.tryParse(picture?['thumbnail'] ?? '');

  DateTime? get dateOfBirth => DateTime.tryParse(dob?['date'] ?? '');

  int? get age => int.tryParse(dob?['age']?.toString() ?? '');

  String? get phones {
    String data = [phone ?? '', cell ?? ''].join(', ');
    return data.isEmpty ? '-' : data;
  }

  @override
  List<Object?> get props => [name, gender, email, picture];
}
