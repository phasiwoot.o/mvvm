class Pagination {
  Pagination({
    this.offset = 0,
    this.limit = 10,
  });

  int offset;
  int limit;

  Pagination copyWith({int? offset, int? limit}) {
    return Pagination(
      offset: offset ?? this.offset,
      limit: limit ?? this.limit,
    );
  }

  void apply({int? offset, int? limit}) {
    this.offset = offset ?? this.offset;
    this.limit = limit ?? this.limit;
  }

  Map<String,String> buildQueryParameter() {
    return <String,String>{
      'page': offset.toString(),
      'results': limit.toString(),
    };
  }
}
