import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mvvm/models/dto/pagination.dart';
import 'package:mvvm/models/employee.dart';
import 'package:mvvm/providers/employee_provider.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

// Add extension
extension StringExtensions on String {
  String capitalize() {
    return '${this[0].toUpperCase()}${substring(1).toLowerCase()}';
  }
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Exam',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: ChangeNotifierProvider<EmployeeProvider>(
        create: (context) => EmployeeProvider(),
        builder: (context, _) {
          return const MyHomePage(title: 'Employees');
        }
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late final EmployeeProvider _employeeProvider;
  late final ScrollController _scrollController;

  bool _visibleRefresh = false;
  bool _visibleScrollToTop = false;

  @override
  void initState() {
    super.initState();

    _employeeProvider = context.read<EmployeeProvider>();
    _scrollController = ScrollController(debugLabel: 'ListScroll')..addListener(_scrollListener);

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _onRefresh();
    });
  }

  @override
  void dispose() {
    _employeeProvider.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  void _onRefresh() {
    _employeeProvider.fetchEmployees(Pagination(offset: 0, limit: 10));
  }

  void _onLoad() {
    final int offset = _employeeProvider.pagination.offset + 1;
    _employeeProvider.fetchEmployees(_employeeProvider.pagination.copyWith(offset: offset));
  }

  void _onScrollToTOp() {
    _scrollController.animateTo(0.0, duration: const Duration(milliseconds: 600), curve: Curves.easeInToLinear);
  }

  void _scrollListener() async {
    double max = _scrollController.position.maxScrollExtent;
    double offset = _scrollController.offset;

    final shouldLoad = _employeeProvider.state == EmployeeState.initial || _employeeProvider.state == EmployeeState.completed || _employeeProvider.state == EmployeeState.error;

    if (max - offset < 50 && shouldLoad) {
      _onLoad();
    }

    final visibleRefresh = offset < 300;
    final visibleScrollToTop = offset >= 300;

    if (visibleRefresh != _visibleRefresh || visibleScrollToTop != _visibleScrollToTop) {
      setState(() {
        _visibleRefresh = visibleRefresh;
        _visibleScrollToTop = visibleScrollToTop;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    const firstItemMargin = EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0, bottom: 20.0);
    const itemMargin = EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0);

    final textStyle = Theme.of(context).textTheme.bodyMedium;
    final labelStyle =textStyle?.copyWith(fontWeight: FontWeight.w700);
    final titleStyle = textStyle?.copyWith(fontSize: 16, fontWeight: FontWeight.w700);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: SizedBox.expand(
        child: Consumer<EmployeeProvider>(
          builder: (_, provider, __) {
            final state = provider.state;
            final items = provider.employees;

            if (state == EmployeeState.loading) {
              return const Center(
                child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.blue))
              );
            } else if (items.isEmpty && state == EmployeeState.completed) {
              return Container(
                alignment: Alignment.topCenter,
                padding: const EdgeInsets.only(top: 80.0),
                child: Image.asset(
                  'assets/no_data.png',
                  width: 100,
                  height: 100,
                  cacheWidth: 100,
                  cacheHeight: 100,
                ),
              );
            }
            return ListView.builder(
              controller: _scrollController,
              itemCount: items.length + 1,
              itemBuilder: (_, index) {
                if (index == items.length && state != EmployeeState.loading) {
                  return const Center(
                    child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.blue))
                  );
                }

                return _itemBuilder(
                  index,
                  items[index],
                  titleStyle: titleStyle,
                  labelStyle: labelStyle,
                  textStyle: textStyle,
                  margin: index == 0 ? firstItemMargin : itemMargin,
                );
              },
              prototypeItem: ListTile(
                title: _itemBuilder(
                  -1,
                  const Employee(id: '', name: 'Mock Item', gender: 'Male', email: 'mock@mock.co'),
                  titleStyle: titleStyle,
                  labelStyle: labelStyle,
                  textStyle: textStyle,
                  margin: firstItemMargin,
                ),
              ),
            );
          },
        ),
      ),
      floatingActionButton: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Visibility(
            visible: _visibleScrollToTop,
            child: FloatingActionButton(
              onPressed: _onScrollToTOp,
              tooltip: 'Scroll to top',
              child: const Icon(Icons.arrow_upward_rounded),
            ),
          ),
          Visibility(
            visible: !_visibleScrollToTop,
            child: FloatingActionButton(
              onPressed: _onRefresh,
              tooltip: 'Refresh',
              child: const Icon(Icons.refresh_rounded),
            ),
          ),
        ],
      ),
    );
  }

  Widget _itemBuilder(int index, Employee item, {
    required EdgeInsetsGeometry margin,
    TextStyle? titleStyle,
    TextStyle? textStyle,
    TextStyle? labelStyle,
  }) {
    return Card(
      margin: margin,
      elevation: 4.0,
      clipBehavior: Clip.none,
      color: Colors.cyan,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 12.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.network(
                item.imageUrl?.toString() ?? '',
                gaplessPlayback: true,
                fit: BoxFit.cover,
                width: 60,
                height: 60,
                cacheWidth: 60,
                cacheHeight: 60,
                errorBuilder: (_, __, ___) {
                  return const SizedBox.square(
                    dimension: 60,
                    child: ColoredBox(
                      color: Colors.white54,
                      child: Center(child: Icon(Icons.no_accounts_rounded)),
                    ),
                  );
                },
              ),
            ),
            const SizedBox(width: 8.0),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Text(item.name, style: titleStyle, maxLines: 1, overflow: TextOverflow.ellipsis),
                  RichWidget(
                      label: 'Gender : ',
                      labelStyle: labelStyle,
                      text: item.gender.capitalize(),
                      textStyle: textStyle
                  ),

                  RichWidget(
                      label: 'Age : ',
                      labelStyle: labelStyle,
                      text: item.age?.toString() ?? '-',
                      textStyle: textStyle
                  ),

                  RichWidget(
                      label: 'Email : ',
                      labelStyle: labelStyle,
                      text: item.email,
                      textStyle: textStyle
                  ),

                  RichWidget(
                      label: 'Tel. : ',
                      labelStyle: labelStyle,
                      text: item.phones ?? '-',
                      textStyle: textStyle
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class RichWidget extends StatelessWidget {
  const RichWidget({
    super.key,
    required this.label,
    this.labelStyle,
    this.text,
    this.defaultText = '-',
    this.textStyle,
  });

  final String label;
  final TextStyle? labelStyle;
  final String? text;
  final String defaultText;
  final TextStyle? textStyle;

  @override
  Widget build(BuildContext context) {
    return RichText(
      maxLines: 2,
      softWrap: true,
      overflow: TextOverflow.ellipsis,
      text: TextSpan(
        text: label,
        style: labelStyle,
        children: [
          TextSpan(text: text ?? defaultText, style: textStyle),
        ],
      ),
    );
  }
}