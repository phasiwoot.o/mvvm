import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:mvvm/models/dto/pagination.dart';
import 'package:mvvm/models/employee.dart';

class EmployeeService {
  static const String _apiEndpoint = 'https://randomuser.me/api';

  static Future<List<Employee>> fetchEmployees(Pagination pagination) async {
    try {
      final response = await http.get(Uri.parse(_apiEndpoint).replace(queryParameters: pagination.buildQueryParameter()));
      if (response.statusCode == 200) {
        final result = jsonDecode(utf8.decode(response.bodyBytes));
        return (result['results'] ?? <Map<String,dynamic>>[]).map<Employee>((e) {
          return Employee.fromJson(e);
        }).toList();
      } else {
        throw Exception('Failed to load data.');
      }
    } catch(e) {
      if (kDebugMode) {
        print(e);
      }
      rethrow;
    }
  }
}